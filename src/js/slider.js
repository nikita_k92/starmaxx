$(document).ready(function () {
  $(".slider-banner").slick(
    {
      dots: true,
      infinite: true,
      speed: 1500,
      slidesToShow: 1,
      adaptiveHeight: true,
      easing: "ease",
      autoplay: false,
      autoplaySpeed: 1500,
      prevArrow: '<button type="button" class="btn btn-slider btn-slider-prev btn-slider-banner"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
      nextArrow: '<button type="button" class="btn btn-slider btn-slider-next btn-slider-banner"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>'
    }
  );
  $(".slider-card-area").slick(
    {
      dots: true,
      infinite: true,
      speed: 1500,
      slidesToShow: 4,
      slidesToScroll: 4,
      adaptiveHeight: true,
      easing: "ease",
      autoplay: false,
      autoplaySpeed: 1500,
      prevArrow: '<button type="button" class="btn btn-slider btn-slider-prev btn-slider-card"><img src="img/prev.png" alt=""></button>',
      nextArrow: '<button type="button" class="btn btn-slider btn-slider-next btn-slider-card"><img src="img/next.png" alt=""></button>',
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    }
  );
  $(".tech-slider").slick(
    {
      dots: true,
      infinite: true,
      speed: 1500,
      slidesToShow: 2,
      slidesToScroll: 2,
      adaptiveHeight: true,
      easing: "ease",
      autoplay: false,
      autoplaySpeed: 1500,
      prevArrow: '<button type="button" class="btn btn-slider btn-slider-prev btn-slider-card"><img src="img/prev.png" alt=""></button>',
      nextArrow: '<button type="button" class="btn btn-slider btn-slider-next btn-slider-card"><img src="img/next.png" alt=""></button>',
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    }
  );
})
