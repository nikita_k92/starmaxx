$(document).ready(function() {
  setTimeout(function() {
    $('#preloader').fadeOut('slow', function() {});
  }, 300);
  let x = [".icon-svg"];
  x.forEach(item => {
    $(item).each(function() {
      let $img = $(this);
      let imgClass = $img.attr("class");
      let imgURL = $img.attr("src");
      $.get(imgURL, function(data) {
        let $svg = $(data).find("svg");
        if (typeof imgClass !== "undefined") {
          $svg = $svg.attr("class", imgClass + " replaced-svg");
        }
        $svg = $svg.removeAttr("xmlns:a");
        if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
          $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"));
        }
        $img.replaceWith($svg);
      }, "");
    });
  });


  let select = $(".select");
  $(select).select2({
    placeholder: "Найти"
  });
  $(".select-city").select2({
    dropdownCssClass: "select-city"
  });
  $("#style-card-row").click(() => {
    $(".style-row").css({ display: "flex" });
    $(".style-card-row").addClass("active");
    $(".style-card-col").removeClass("active");
    $(".style-col").css({ display: "none" });
  });


  $("#style-card-col").click(() => {
    $(".style-row").css({ display: "none" });
    $(".style-card-row").removeClass("active");
    $(".style-card-col").addClass("active");
    $(".style-col").css({ display: "flex" });
  });



  $("[data-range]").each(function(index, elem) {
    let obj = $(elem),
      step = obj.data("step"),
      minValue = obj.data("range-min"),
      maxValue = obj.data("range-max"),
      parent = obj.parents("[data-parent]").first(),
      inputFrom = parent.find("[data-range-from]").first(),
      inputTo = parent.find("[data-range-to]").first(),
      fromValue = (inputFrom.val()) ? inputFrom.val() : minValue,
      toValue = (inputTo.val()) ? inputTo.val() : maxValue;

    obj.slider({
      min: minValue,
      max: maxValue,
      step: step,
      animate: "slow",
      range: true,
      values: [fromValue, toValue],
      slide: function(event, ui) {
        inputFrom.val(ui.values[0]);
        inputTo.val(ui.values[1]);

        inputTo.trigger("keyup");
      }
    });

    inputFrom.val(obj.slider("values", 0));
    inputTo.val(obj.slider("values", 1));
  });
  /**/
  //хуйня раскрываюшая карту в модалке адресов
  $("[name=\"btn-map-1\"]").click(() => {
    $("[name=\"map-1\"]").slideToggle();
  });

  $('.close-window').click(()=>{
    $(".drop-down-order-call").removeAttr("data-show")
  })

  let tooltip = $('.tooltip').toArray()

   $(window).click(function(e) {
    if ($(e.target).closest(".tooltip").length === 0) {
      tooltip.forEach(x=>{
        $(x).removeAttr("data-show")
      })
    }
  });

});
