const tooltipType = (event, el1, el2, offsetX, offsetY, position) => {
  event.stopPropagation();
  el2.setAttribute("data-show", "");
  Popper.createPopper(el1, el2, {
    placement: position,
    modifiers: [
      {
        name: "offset",
        options: {
          offset: [offsetX, offsetY]
        }
      }
    ]
  });
};
const button = document.querySelector("[data-name=\"nav-search\"]");
const tooltip = document.querySelector("[data-name=\"tooltip-search\"]");

const buttonCart = document.querySelector("[data-name=\"header-cart\"]");
const tooltipCart = document.querySelector("[data-name=\"tooltip-header-cart\"]");

const buttonMapPoint3 = document.querySelector("[data-name=\"map-point-3\"]");
const tooltipMapPoint3 = document.querySelector("[data-name=\"tooltip-map-point-3\"]");

const buttonOrderCall = document.querySelector("[data-name=\"order-call\"]");
const tooltipOrderCAll = document.querySelector("[data-name=\"tooltip-order-call\"]");

button.addEventListener("click", e => tooltipType(e,button, tooltip, 0, -23, "left"));
buttonCart.addEventListener("click", e => tooltipType(e,buttonCart, tooltipCart, 0, 10, "bottom"));
buttonOrderCall.addEventListener("click", e => tooltipType(e,buttonOrderCall, tooltipOrderCAll, 0, 10, "bottom"));
if(buttonMapPoint3){
  buttonMapPoint3.addEventListener("click", e => tooltipType(e,buttonMapPoint3, tooltipMapPoint3, 0, 15, "top"));
}
$("#order-call").click(function(e) {
  tooltipType(e,buttonOrderCall, tooltipOrderCAll, 0, 10, "bottom")

  window.scrollTo({ top: 0, behavior: "smooth" });
});
